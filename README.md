# Mini Project 9: Streamlit App with a Hugging Face Model

## Accessing the Deployed App
[https://ids721mini9-dl402.streamlit.app](https://ids721mini9-dl402.streamlit.app).
The demo video is in the root folder of this repo.

## Project Description
This project highlights the seamless incorporation of an advanced Language Model from Hugging Face into a Streamlit web application, illustrating the capabilities of open-source language models. It allows users to input prompts, utilizing the GPT-2 model to produce a wide array of context-sensitive outputs, from storytelling to creating simulated conversations. The aim is to demonstrate the versatility and potential of language models in generating varied and relevant text responses.

## Objectives
- Creating a website using Streamlit
- Connecting to an open source LLM (Hugging Face)
- Deploying the model via Streamlit

## Environments
1. Confirm that Python version 3.9 or newer is installed on your machine.
2. Execute the following command to install Streamlit along with necessary dependencies:
   ```bash
   pip3 install streamlit transformers tensorflow tf-keras
   ```
3. To check the successful installation, execute:
    ```bash
    streamlit hello
    ```

### Streamlit Web Application
1. Generate a Python script `LLM.py` to serve as the foundation for the Streamlit application.
2. Utilize the `transformers` library to integrate the GPT-2 model.
3. Design a user interface within the app that allows for the input of text prompts and showcases the resulting generated text. In my case is in dark mode.

### Local Testing
To test the application locally, execute:
```bash
streamlit run LLM.py
```
Go to `http://localhost:8501` in your web browser to test the functionalities.

### Deployment on Streamlit Cloud
1. Create a `requirements.txt` file with the following content to manage dependencies:
```plaintext
streamlit
transformers
tensorflow
tf-keras
```

This is used when streamlit runs your app. It will install the packages based on the contents in your requirements.txt.

2. Push your project to a GitHub repository since it does not support directly link to gitlab repos.
3. Sign up on [Streamlit Cloud](https://streamlit.io/).
4. Choose "New app", then select your `repository` and the `branch` where `LLM.py` is located and specify your `main file path`, which is `LLM.py` in my case.
5. Click "Deploy!" to launch your app. 

## Screenshots 
   ![local](screenshot.png)
